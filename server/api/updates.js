const Router = require('express-promise-router');

const db = require('../db');

const updates = new Router();

updates.get('/:id', async (req, res) => {
  const { id } = req.params;
  const { rows } = await db.query('SELECT * FROM updates WHERE update_id = $1', [id]);
  if (!rows.length) return res.sendStatus(404);
  return res.json(rows[0]);
});

updates.post('/', async (req, res) => {
  const { message, loc } = req.body;
  const { user_id } = req.user;

  await db.query(
    'INSERT INTO updates (message, loc, user_id) VALUES ($1, ST_MakePoint($2, $3), $4)',
    [message, loc.longitude, loc.latitude, user_id]
  );

  return res.sendStatus(200);
});

module.exports = updates;
