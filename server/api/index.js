const express = require('express');
const passport = require('passport');

const updates = require('./updates');
const users = require('./users');
const follows = require('./follows')
const tokens = require('./tokens');

const api = new express.Router();
api.use('/updates', passport.authenticate('jwt', { session: false }), updates);
api.use('/users', users);
api.use('/follows', passport.authenticate('jwt', { session: false }), follows);
api.use('/tokens', tokens);

module.exports = api;
