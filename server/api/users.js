const passport = require('passport');
const bcrypt = require('bcryptjs');
const { middleware: paginateMiddleware } = require('express-paginate');
const Router = require('express-promise-router');

const db = require('../db');

const users = new Router();
const paginate = paginateMiddleware(10, 50);

users.post('/:id/follow', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { id } = req.params;
  const { user_id } = req.user;

  try {
    const { rows: [follow_id] } = await db.query(`
      INSERT INTO follows (follower_id, following_id, accepted)
      VALUES ($1, $2, NULL) ON CONFLICT (follower_id, following_id) DO NOTHING
      RETURNING follow_id
    `, [user_id, id]);

    return res.json({ follow_id });
  }
  catch (err) {
    return res.sendStatus(500);
  }
});

users.post('/:id/unfollow', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { id } = req.params;
  const { user_id } = req.user;

  try {
    await db.query('DELETE FROM follows WHERE follower_id = $1 AND following_id = $2', [user_id, id]);
    return res.sendStatus(200);
  }
  catch (err) {
    return res.sendStatus(500);
  }
});

users.get('/me', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { user_id } = req.user;
  const { rows } = await db.query(`
    SELECT followers_count, following_count FROM
      (SELECT COUNT(*) AS followers_count FROM follows
       WHERE following_id = $1 AND accepted) AS followers,
      (SELECT COUNT(*) AS following_count FROM follows
       WHERE follower_id = $1 AND accepted) AS following
  `, [user_id]);

  let [{ followers_count, following_count }] = rows;
  followers_count = parseInt(followers_count, 10);
  following_count = parseInt(following_count, 10);

  return res.json({ followers_count, following_count, ...req.user });
});

users.get('/me/updates', passport.authenticate('jwt', { session: false }), paginate, async (req, res) => {
  const { user_id } = req.user;
  const { limit, skip } = req.query;
  const { rows } = await db.query(`
    SELECT *, ST_X(loc::geometry) AS lon, ST_Y(loc::geometry) AS lat
    FROM updates INNER JOIN users ON users.user_id = updates.user_id
    WHERE users.user_id = $1
    ORDER BY created DESC
    LIMIT $2 OFFSET $3
  `, [user_id, limit, skip]);

  const updates = rows.map(({ passwd, loc, lon, lat, ...row }) => ({
    ...row,
    loc: { lon, lat }
  }));
  return res.json({ updates });
});

users.get('/me/timeline', passport.authenticate('jwt', { session: false }), paginate, async (req, res) => {
  const { user_id } = req.user;
  const { limit, skip } = req.query;
  const { rows } = await db.query(`
    WITH relevant_users AS (
      SELECT users.*
      FROM users LEFT JOIN follows ON users.user_id = follows.following_id
      WHERE (follows.follower_id = $1 AND accepted) OR users.user_id = $1
    )
    SELECT *, ST_X(loc::geometry) AS lon, ST_Y(loc::geometry) AS lat
    FROM updates INNER JOIN relevant_users ON updates.user_id = relevant_users.user_id
    ORDER BY created DESC
    LIMIT $2 OFFSET $3
  `, [user_id, limit, skip]);

  const updates = rows.map(({ passwd, loc, lon, lat, ...row }) => ({
    ...row,
    loc: { lon, lat }
  }));
  return res.json({ updates });
});

users.get('/me/follows', passport.authenticate('jwt', { session: false }), paginate, async (req, res) => {
  const { user_id } = req.user;
  const { limit, skip } = req.query;

  const { rows } = await db.query(`
    SELECT *
    FROM follows INNER JOIN users ON follows.follower_id = users.user_id
    WHERE following_id = $1
    ORDER BY created DESC
    LIMIT $2 OFFSET $3
  `, [user_id, limit, skip]);
  const follows = rows.map(({ passwd, ...rest }) => rest);

  return res.json({ follows });
})

users.get('/:id', passport.authenticate('jwt', { session: false }), async (req, res) => {
  const { user_id } = req.user;
  const { id } = req.params;

  const [userInfoQuery, followCountsQuery, followingQuery] = await Promise.all([
    db.query(
      'SELECT user_id, username FROM users WHERE user_id = $1',
      [id]
    ),
    db.query(`
      SELECT followers_count, following_count FROM
        (SELECT COUNT(*) AS followers_count FROM follows
         WHERE following_id = $1 AND accepted) AS followers,
        (SELECT COUNT(*) AS following_count FROM follows
         WHERE follower_id = $1 AND accepted) AS following
    `, [id]),
    db.query(`
      SELECT follow_id, accepted FROM follows WHERE following_id = $1 AND follower_id = $2
    `, [id, user_id])
  ]);

  if (!userInfoQuery.rows.length) return res.send(404);

  const user = userInfoQuery.rows[0];
  const { followers_count, following_count } = followCountsQuery.rows[0];
  const follow_id = followingQuery.rows.length > 0 ? followingQuery.rows[0].follow_id : null;
  const accepted = follow_id !== null ? followingQuery.rows[0].accepted : false;

  return res.json({
    ...user,
    followers_count,
    following_count,
    follow_id,
    accepted
  });
});

users.get('/:id/updates', passport.authenticate('jwt', { session: false }), paginate, async (req, res) => {
  const { id } = req.params;
  const { user_id } = req.user;
  const { limit, skip } = req.query;

  if (id != user_id) {
    const { rows } = await db.query(`SELECT EXISTS
      (SELECT 1 FROM follows WHERE follower_id = $1 AND following_id = $2 AND accepted LIMIT 1)
    `, [user_id, id]);
    const { exists: following } = rows[0];
    if (!following) return res.sendStatus(401);
  }

  const { rows } = await db.query(`
    SELECT *, ST_X(loc::geometry) AS lon, ST_Y(loc::geometry) AS lat
    FROM updates INNER JOIN users ON users.user_id = updates.user_id
    WHERE users.user_id = $1
    ORDER BY created DESC
    LIMIT $2 OFFSET $3
  `, [id, limit, skip]);

  const updates = rows.map(({ passwd, loc, lon, lat, ...row }) => ({
    ...row,
    loc: { lon, lat }
  }));
  return res.json({ updates });
});

users.get('/', paginate, async (req, res) => {
  const { filter, limit, skip } = req.query;

  try {
    const { rows } = await db.query(`
      SELECT * FROM users
      WHERE username ILIKE $1
      LIMIT $2 OFFSET $3
    `, [`%${filter}%`, limit, skip]);
    const users = rows.map(({ passwd, ...rest }) => rest);

    return res.json({ users });
  }
  catch (err) {
    return res.sendStatus(500);
  }
});

users.post('/', async (req, res) => {
  const { username, password } = req.body;
  const salt = bcrypt.genSaltSync(10);
  const hash = bcrypt.hashSync(password, salt);

  try {
    await db.query('INSERT INTO users (username, passwd) VALUES ($1, $2)', [username, hash]);
    return res.sendStatus(200);
  }
  catch (err) {
    if (err.code === '23505') { // unique_violation
      return res.sendStatus(409);
    }
    return res.sendStatus(500);
  }
});

module.exports = users;
