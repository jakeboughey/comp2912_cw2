const passport = require('passport');
const Router = require('express-promise-router');

const db = require('../db');

const follows = new Router();

async function setAcceptedStatus(id, status) {
  await db.query('UPDATE follows SET accepted = $1 WHERE follow_id = $2', [status, id]);
}

follows.post('/:id/accept', async (req, res) => {
  try {
    await setAcceptedStatus(req.params.id, true);
    return res.sendStatus(200);
  }
  catch (err) {
    return res.sendStatus(500);
  }
});

follows.post('/:id/ignore', async (req, res) => {
  try {
    await setAcceptedStatus(req.params.id, false);
    return res.sendStatus(200);
  }
  catch (err) {
    return res.sendStatus(500);
  }
});

module.exports = follows;
