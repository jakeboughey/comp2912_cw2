const Router = require('express-promise-router');
const passport = require('passport');
const jwt = require('jsonwebtoken');

const tokens = new Router();

const generateJWTForUser = (sub, cb) => jwt.sign(
  { sub },
  process.env.SECRET,
  { expiresIn: process.env.DEFAULT_TOKEN_LENGTH },
  (err, token) => {
    if (err) cb(err);
    else cb(null, token);
  }
);

// Authenticates a user via JSON credentials and sends a JWT that encodes their ID
tokens.post('/new', passport.authenticate('json', { session: false }), async (req, res) => {
  generateJWTForUser(req.user.user_id, (err, token) => {
    if (err) res.sendStatus(500);
    else res.json({ token });
  });
});

// Given an already valid JWT returns a new one
tokens.post('/refresh', async (req, res) => {
  jwt.verify(req.body.token, process.env.SECRET, (err, decoded) => {
    if (err) return res.sendStatus(401);
    else generateJWTForUser(decoded.sub, (err, token) => {
      if (err) res.sendStatus(500);
      else res.json({ token });
    });
  });
});

module.exports = tokens
