const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcryptjs');
const logger = require('morgan');
const passport = require('passport');
const JSONStrategy = require('passport-json');
const { Strategy: JWTStrategy, ExtractJwt } = require('passport-jwt')

require('dotenv').config();

const db = require('./db');

passport.use(new JSONStrategy(
  { passReqToCallback: true },
  (req, username, password, done) => {
    db.query('SELECT * FROM users WHERE username = $1', [username])
      .then(({ rows }) => {
        if (!rows.length) return done(null, false);
        const { passwd, ...user } = rows[0];
        const match = bcrypt.compareSync(password, passwd);
        return done(null, match && user);
      })
      .catch(err => done(err));
  }
));

const jwtOpts = {
  jwtFromRequest: ExtractJwt.fromAuthHeaderWithScheme('JWT'),
  secretOrKey: process.env.SECRET
}

passport.use(new JWTStrategy(jwtOpts,
  ({ sub: id }, done) => {
    db.query('SELECT * FROM users WHERE user_id = $1', [id])
      .then(({ rows }) => {
        if (!rows.length) return done(null, false);

        const { passwd, ...user } = rows[0];
        return done(null, user);
      })
      .catch(err => done(err));
  }
))

const app = express();

app.use(passport.initialize());
app.use(bodyParser.json());
app.use(logger('combined'));

const api = require('./api');
app.use('/api', api);

app.listen(8082);
