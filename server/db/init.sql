SET TIME ZONE 'UTC';
CREATE DATABASE comp2912;
\connect comp2912
CREATE EXTENSION postgis;

CREATE TABLE users(
  user_id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  username text UNIQUE,
  passwd text
);

CREATE TABLE follows(
  follow_id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  created timestamp with time zone NOT NULL DEFAULT NOW(),
  follower_id integer REFERENCES users(user_id),
  following_id integer REFERENCES users(user_id),
  accepted boolean, -- true = req. accepted, false = req. rejected, null = yet to decide
  CONSTRAINT unique_follower_following UNIQUE (follower_id, following_id)
);

CREATE TABLE updates(
  update_id integer PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
  created timestamp with time zone NOT NULL DEFAULT NOW(),
  message text,
  loc geography,
  user_id integer REFERENCES users(user_id)
);
