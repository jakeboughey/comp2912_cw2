import React from 'react';
import { Text } from 'react-native';
import MapView, { Marker } from 'react-native-maps';

import Link from './Link';

function UpdateCard({ user_id, username, loc, message, unlinked = false }) {
  const PossiblyLink = unlinked ? Text : Link;
  const location = {
    longitude: loc.lon,
    latitude: loc.lat
  };

  return (
    <>
      <PossiblyLink
        style={{ fontWeight: 'bold' }}
        to="Profile"
        params={{ id: user_id }}
      >
        {username}:
      </PossiblyLink>
      <Text>{message}</Text>
      <MapView
        style={{ height: 200 }}
        rotateEnabled={false}
        scrollEnabled={false}
        zoomEnabled={false}
        pitchEnabled={false}
        initialRegion={{ ...location, latitudeDelta: 0.01, longitudeDelta: 0.01 }}
      >
        <Marker coordinate={location} />
      </MapView>
    </>
  );
}

export default UpdateCard;
