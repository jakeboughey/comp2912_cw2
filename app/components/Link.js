import React from 'react';
import { Text } from 'react-native';
import { withNavigation } from 'react-navigation';

function Link({ to, params, navigation, ...rest }) {
  return <Text {...rest} onPress={() => navigation.navigate(to, params)} />;
}

export default withNavigation(Link);
