import React, { useState, useEffect } from 'react';
import { FlatList } from 'react-native';

import { getApi } from '../util/api';

function InfiniteScrollList({
  url,
  query = '',
  limit = 10,
  initialSkip = 0,
  extractData = json => json,
  ...rest
}) {
  const [skip, setSkip] = useState(initialSkip);
  const [stale, setStale] = useState(true);
  const [data, setData] = useState([]);

  useEffect(() => {
    setSkip(initialSkip);
    setData([]);
  }, [url, query]);

  useEffect(function() {
    async function getPage() {
      const res = await getApi(
        `${url}?${query}${query && '&'}limit=${limit}&skip=${skip}`
      );
      const newData = extractData(await res.json());

      setData([...data, ...newData]);
      setStale(false);
    }

    getPage();
  }, [skip]);

  return (
    <FlatList
      data={data}
      onEndReached={() => {
        setSkip(skip + limit);
      }}
      onRefresh={() => {
        setStale(true);
        setData([]);
        setSkip(initialSkip);
      }}
      refreshing={stale}
      {...rest}
    />
  );
}

export default InfiniteScrollList;
