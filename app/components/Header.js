import React from 'react';
import { Text } from 'react-native';

function Header({ style = {}, ...rest }) {
  return <Text style={{ fontSize: 24, fontWeight: 'bold', ...style }} {...rest} />
}

export default Header;
