import React from 'react';
import { View, Button, Text } from 'react-native';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';
import { createStackNavigator } from 'react-navigation-stack';
import { createBottomTabNavigator } from 'react-navigation-tabs';

import HomeScreen from './views/HomeScreen';
import SearchScreen from './views/SearchScreen';
import PostScreen from './views/PostScreen';
import FollowsScreen from './views/FollowsScreen';
import ProfileScreen from './views/ProfileScreen';
import LoginScreen from './views/LoginScreen';
import RegisterScreen from './views/RegisterScreen';
import AuthLoadingScreen from './views/AuthLoadingScreen';

const HomeNavigator = createStackNavigator(
  {
    Home: HomeScreen,
    Profile: ProfileScreen
  },
  {
    initialRouteName: 'Home'
  }
);

const SearchNavigator = createStackNavigator(
  {
    Search: SearchScreen,
    Profile: ProfileScreen
  },
  {
    initialRouteName: 'Search'
  }
);

const AppNavigator = createBottomTabNavigator(
  {
    Home: HomeNavigator,
    Search: SearchNavigator,
    Post: PostScreen,
    Notifications: FollowsScreen,
    Profile: ProfileScreen
  },
  {
    initialRouteName: 'Home'
  }
);

const AuthNavigator = createStackNavigator(
  {
    Login: LoginScreen,
    Register: RegisterScreen
  },
  {
    initialRouteName: 'Login'
  }
);

const MainNavigator = createSwitchNavigator(
  {
    AuthLoading: AuthLoadingScreen,
    Auth: AuthNavigator,
    App: AppNavigator
  },
  {
    initialRouteName: 'AuthLoading'
  }
);

const AppContainer = createAppContainer(MainNavigator);
const App = () => <AppContainer />;

export default App;
