import Config from 'react-native-config';
import AsyncStorage from '@react-native-community/async-storage';

export async function fetchApi(url, opts = {}) {
  const { headers, ...rest } = opts;
  const token = await AsyncStorage.getItem('userToken');

  return fetch(`${Config.API_ROOT}${url}`, {
    headers: {
      ...token && { 'Authorization': `JWT ${token}` },
      ...headers
    },
    ...rest
  });
}

export const getApi = fetchApi;

export function postApi(url, opts = {}) {
  return fetchApi(url, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json'
    },
    ...opts
  });
}
