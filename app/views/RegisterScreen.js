import React, { useState } from 'react';
import { SafeAreaView, Text, TextInput, Button } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import Header from '../components/Header';
import { postApi } from '../util/api';

function RegisterScreen({ navigation }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  async function trySubmit() {
    const body = JSON.stringify({ username, password });
    const newUserRes = await postApi('/users', { body });

    if (newUserRes.ok) {
      const tokenRes = await postApi('/tokens/new', { body });

      if (tokenRes.ok) {
        const { token } = await tokenRes.json();

        await AsyncStorage.setItem('userToken', token);
        navigation.navigate('Home');
      }
    }
  }

  return (
    <SafeAreaView>
      <Header>Register</Header>

      <Text>Username</Text>
      <TextInput
        autoCapitalize="none"
        placeholder="Username"
        value={username}
        onChangeText={text => setUsername(text)}
      />

      <Text>Password</Text>
      <TextInput
        secureTextEntry
        placeholder="Password"
        value={password}
        onChangeText={text => setPassword(text)}
      />

      <Button title="Submit" onPress={trySubmit} />
    </SafeAreaView>
  );
}

export default RegisterScreen;
