import React, { useEffect } from 'react';
import { SafeAreaView, ActivityIndicator } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import { postApi } from '../util/api';

function AuthLoadingScreen({ navigation }) {
  useEffect(() => {
    async function tryRefreshToken() {
      const token = await AsyncStorage.getItem('userToken');

      if (token) {
        const res = await postApi('/tokens/refresh', {
          body: JSON.stringify({ token })
        });
        if (!res.ok) return navigation.navigate('Auth');

        const { token: newToken } = await res.json();

        try {
          await AsyncStorage.setItem('userToken', newToken);
          navigation.navigate('App');
        }
        catch (err) {
          navigation.navigate('Login');
        }
      }
      else navigation.navigate('Login');
    }

    tryRefreshToken();
  }, []);

  return (
    <SafeAreaView>
      <ActivityIndicator />
    </SafeAreaView>
  );
}

export default AuthLoadingScreen;
