import React, { useEffect, useState, useMemo } from 'react';
import { SafeAreaView, Button, Text } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import InfiniteScrollList from '../components/InfiniteScrollList';
import UpdateCard from '../components/UpdateCard';
import Header from '../components/Header';
import { getApi, postApi } from '../util/api';

function ProfileScreen({ navigation }) {
  const [{
    user_id,
    username,
    followers_count,
    following_count,
    accepted
  }, setProfile] = useState({});
  const [stale, setStale] = useState(true);
  const [followId, setFollowId] = useState(null);
  const id = navigation.getParam('id', 'me');

  const { followAction, onPressFollowBtn } = useMemo(() => {
    if (followId !== null) {
      return {
        followAction: accepted ? 'Unfollow' : 'Requested',
        onPressFollowBtn: async () => {
          const res = await postApi(`/users/${id}/unfollow`);
          if (res.ok) {
            setFollowId(null);
            setStale(true);
          }
        }
      };
    }
    else {
      return {
        followAction: 'Follow',
        onPressFollowBtn: async () => {
          const res = await postApi(`/users/${id}/follow`);
          if (res.ok) {
            const { follow_id } = await res.json();
            setFollowId(follow_id);
            setStale(true);
          }
        }
      };
    }
  }, [followId, accepted, id]);

  useEffect(() => {
    if (!stale) return;

    async function getUserInfo() {
      const res = await getApi(`/users/${id}`);
      const { follow_id, ...rest } = await res.json();

      setFollowId(follow_id);
      setProfile(rest);
      setStale(false);
    }

    getUserInfo();
  }, [stale]);

  async function logOut() {
    await AsyncStorage.removeItem('userToken');
    navigation.navigate('AuthLoading');
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header>{username}</Header>
      <Text>{following_count} following</Text>
      <Text>{followers_count} followers</Text>

      {id === 'me' ? (
        <Button title="Log out" onPress={logOut} />
      ) : (
        <Button title={followAction} onPress={onPressFollowBtn} />
      )}

      {(id === 'me' || accepted) && (
        <InfiniteScrollList
          url={`/users/${id}/updates`}
          style={{ flex: 1 }}
          renderItem={({ item }) => <UpdateCard unlinked {...item} />}
          keyExtractor={item => item.update_id.toString()}
          extractData={json => json.updates}
        />
      )}
    </SafeAreaView>
  );
}

ProfileScreen.navigationOptions = () => ({ title: 'Profile' });

export default ProfileScreen;
