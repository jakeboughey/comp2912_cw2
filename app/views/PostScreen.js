import React, { useState, useEffect } from 'react';
import {
  Dimensions,
  StyleSheet,
  View,
  Text,
  Button,
  TextInput,
  Switch
} from 'react-native';
import Geolocation from 'react-native-geolocation-service';
import MapView, { Marker } from 'react-native-maps';

import { postApi } from '../util/api';

function PostScreen({ navigation }) {
  const [location, setLocation] = useState(null);
  const [message, setMessage] = useState('');

  useEffect(() => {
    Geolocation.getCurrentPosition(
      pos => {
        setLocation({
          latitude: pos.coords.latitude,
          longitude: pos.coords.longitude
        });
      },
      err => console.error(err)
    );
  }, []);

  async function trySubmitPost() {
    await postApi('/updates', {
      body: JSON.stringify({ message, loc: location })
    });
    navigation.navigate('Home');
  }

  return (
    <View>
      {location &&
        <MapView
          style={{ height: 400 }}
          initialRegion={{ ...location, latitudeDelta: 0.01, longitudeDelta: 0.01 }}
        >
        <Marker coordinate={location} />
      </MapView>}
      <TextInput
        value={message}
        placeholder="Type a message..."
        onChangeText={text => setMessage(text)}
      />
      <Button title="Post" onPress={trySubmitPost} />
    </View>
  );
}

PostScreen.navigationOptions = () => ({ title: 'Post' });

export default PostScreen;
