import React, { useEffect, useState } from 'react';
import { SafeAreaView, Button, FlatList } from 'react-native';

import InfiniteScrollList from '../components/InfiniteScrollList';
import UpdateCard from '../components/UpdateCard';
import Header from '../components/Header';
import { fetchApi } from '../util/api';

function HomeScreen({ navigation }) {
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header>Home</Header>
      <InfiniteScrollList
        url="/users/me/timeline"
        style={{ flex: 1 }}
        renderItem={({ item }) => <UpdateCard {...item} />}
        keyExtractor={item => item.update_id.toString()}
        extractData={json => json.updates}
      />
    </SafeAreaView>
  );
}
HomeScreen.navigationOptions = () => ({ title: 'Home', header: null });

export default HomeScreen;
