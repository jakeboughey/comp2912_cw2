import React, { useState } from 'react';
import { SafeAreaView, Text, Button } from 'react-native';

import Header from '../components/Header';
import InfiniteScrollList from '../components/InfiniteScrollList';
import { postApi } from '../util/api';

function FollowRequestCard({ username, accepted, onAcceptFollow, onIgnoreFollow }) {
  return (
    <>
      <Text>{username}</Text>
      {accepted === null ? (
        <>
          <Button title="Accept" onPress={onAcceptFollow} />
          <Button title="Ignore" onPress={onIgnoreFollow} />
        </>
      ) : (
        accepted ? <Text>✓ Accepted</Text> : <Text>╳ Ignored</Text>
      )}
    </>
  );
}

function FollowsScreen({ navigation }) {
  const [follows, setFollows] = useState([]);

  async function onAcceptFollow(id) {
    const res = await postApi(`/follows/${id}/accept`);
    if (res.ok) setFollows([...follows, { id, accepted: true }]);
  }

  async function onIgnoreFollow(id) {
    const res = await postApi(`/follows/${id}/ignore`);
    if (res.ok) setFollows([...follows, { id, accepted: false }]);
  }

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header>Follows</Header>
      <InfiniteScrollList
        url="/users/me/follows"
        style={{ flex: 1 }}
        extractData={json => json.follows}
        extraData={follows}
        renderItem={({ item }) => {
          const updatedFollow = follows.find(follow => follow.id === item.follow_id);

          return (
            <FollowRequestCard
              {...item}
              accepted={typeof updatedFollow !== 'undefined' ? updatedFollow.accepted : item.accepted}
              onAcceptFollow={() => onAcceptFollow(item.follow_id)}
              onIgnoreFollow={() => onIgnoreFollow(item.follow_id)}
            />
          );
        }}
        keyExtractor={item => item.follow_id.toString()}
      />
    </SafeAreaView>
  );
}

FollowsScreen.navigationOptions = () => ({ title: 'Follows' });

export default FollowsScreen;
