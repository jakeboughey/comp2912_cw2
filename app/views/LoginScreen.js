import React, { useState } from 'react';
import { SafeAreaView, Text, TextInput, Button } from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';

import Header from '../components/Header';
import { postApi } from '../util/api';

function LoginScreen({ navigation }) {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');

  async function tryGetToken() {
    const res = await postApi('/tokens/new', {
      body: JSON.stringify({ username, password })
    });

    if (res.ok) {
      const { token } = await res.json();
      await AsyncStorage.setItem('userToken', token);

      navigation.navigate('App');
    }
  }

  return (
    <SafeAreaView>
      <Header>Login</Header>

      <Text>Username</Text>
      <TextInput
        autoCapitalize="none"
        placeholder="Username"
        value={username}
        onChangeText={text => setUsername(text)}
      />

      <Text>Password</Text>
      <TextInput
        secureTextEntry
        placeholder="Password"
        value={password}
        onChangeText={text => setPassword(text)}
      />

      <Button title="Log in" onPress={tryGetToken} />
      <Button title="Register" onPress={() => navigation.navigate('Register')} />
    </SafeAreaView>
  );
}
LoginScreen.navigationOptions = () => ({ header: null });

export default LoginScreen;
