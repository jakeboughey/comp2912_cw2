import React, { useState, useEffect } from 'react';
import { SafeAreaView, TextInput, Text } from 'react-native';

import Link from '../components/Link';
import Header from '../components/Header';
import InfiniteScrollList from '../components/InfiniteScrollList';

function SearchScreen({ navigation }) {
  const [filter, setFilter] = useState(null);
  const [results, setResults] = useState([]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Header>Search</Header>

      <TextInput
        autoCapitalize="none"
        placeholder="Search users..."
        value={filter}
        onChangeText={setFilter}
      />

      {filter !== null && (
        <InfiniteScrollList
          url="/users/"
          query={`filter=${filter}`}
          style={{ flex: 1 }}
          renderItem={({ item }) => <Link to="Profile" params={{ id: item.user_id }}>{item.username}</Link>}
          keyExtractor={item => item.user_id.toString()}
          extractData={json => json.users}
        />
      )}
    </SafeAreaView>
  );
}
SearchScreen.navigationOptions = () => ({ title: 'Search', header: null });

export default SearchScreen;
