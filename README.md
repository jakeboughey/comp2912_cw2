# COMP2912 Software Engineering Principles CW2

Instagram-like app used to share and subscribe to user-made posts containing images, text, location data etc.

## Getting started

```
$ git clone gitlab.com/jakeboughey/comp2912_cw2
$ cd comp2912_cw2
$ cd server && npm install
$ cd ../app && npm install
```

If running for the first time, in order to set up the database ensure Postgres is running with PostGIS installed then do

```
$ psql -f server/db/init.sql
```

To run the server:

```
$ cd server && node index.js
```

To run the app, ensure the server is running then do:

```
$ cd app && react-native run-ios
```

or

```
$ cd app && react-native run-android
```
